import React, { useState } from 'react';

function ShoeForm() {
  const [shoeData, setShoeData] = useState({
    manufacturer: '',
    modelName: '',
    color: '',
    pictureUrl: ''
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setShoeData({ ...shoeData, [name]: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    console.log(shoeData);

    // Reset form fields
    setShoeData({
      manufacturer: '',
      modelName: '',
      color: '',
      pictureUrl: ''
    });
  };

  return (
    <div>
      <h2>Add New Shoe</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Manufacturer:</label>
          <input
            type="text"
            name="manufacturer"
            value={shoeData.manufacturer}
            onChange={handleChange}
          />
        </div>
        <div>
          <label>Model Name:</label>
          <input
            type="text"
            name="modelName"
            value={shoeData.modelName}
            onChange={handleChange}
          />
        </div>
        <div>
          <label>Color:</label>
          <input
            type="text"
            name="color"
            value={shoeData.color}
            onChange={handleChange}
          />
        </div>
        <div>
          <label>Picture URL:</label>
          <input
            type="text"
            name="pictureUrl"
            value={shoeData.pictureUrl}
            onChange={handleChange}
          />
        </div>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default ShoeForm;
