import { useEffect, useState } from "react";
function HatColumn(props) {

  return (
    <div className="col">
      {props.hatList.map(hat => {


        return (
          <div key={hat.href} className="card mb-3 shadow">
            <img src={hat.picture_url} className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">{hat.style}</h5>
              <h6 className="card-subtitle mb-2 text-muted">

              </h6>
              <h6 className="card-title">Color: {hat.color}</h6>
              <h6 className="card-title">Fabric: {hat.fabric}</h6>
              <button
        className="k-button" value={hat.href}
        onClick={() => props.handleRemove(hat)}
      >
        Remove
      </button>

            </div>
          </div>
        );
      })}
    </div>
  );
}

const HatsList = (props) => {
    const [hatColumns, setHatColumns] = useState([[], [], []]);

    const getHats = async function() {
      const url = 'http://localhost:8090/hats/'
    //   const response = await fetch(url)
    //   if (response.ok) {
    //     const{hats} = await response.json();
    //     setHats(hats)
    //   }

    try {
      const response = await fetch(url);
      if (response.ok) {
        // Get the list of hats
        const data = await response.json();

        // Create a list of for all the requests and
        // add all of the requests to it
        // console.log(data.hats)
        // console.log(data)



        const requests = [];
        for (let hat of data.hats) {
          const detailUrl = `http://localhost:8090${hat.href}`;
          requests.push(fetch(detailUrl));
        }
        const responses = await Promise.all(requests);
        const columns = [[],[], []]

        let i = 0;
        for (const hatResponse of responses) {
          if (hatResponse.ok) {
            const details = await hatResponse.json();

            columns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(hatResponse);
          }
        }


      setHatColumns(columns);
    }
  } catch (e) {
    console.error(e);
  }
}

const handleRemove = async (hat) => {

  const url = 'http://localhost:8090' +hat.href;
  const fetchConfig = {
    method: 'delete',
    headers: {
      'Content-Type' : 'application/json',
    }
  }

  const response = await fetch(url, fetchConfig)
  console.log(response)
  if (response.ok){

    getHats();
  }


}




    useEffect(() => {
      getHats();
    }, []);
return (


  <>
  <div>

    <h1 className="display-5 fw-bold">Hats</h1>

  </div>
  <div className="container">
    <div className="row">
      {hatColumns.map((hatList, href) => {
        return (

          <HatColumn key={href} hatList={hatList}  handleRemove={handleRemove}/>
        );
      })}
    </div>
  </div>
  </>
  );
  }

export default HatsList;
//     <table className="table table-striped">
//         <thead>
//           <tr>
//             <th>Style</th>
//             <th>Color</th>
//             <th>Location</th>
//             <th>Shelf Number</th>

//           </tr>
//         </thead>
//         <tbody>

//           {hats?.map(hat => {
//             return (

//               <tr key={hat.href}>
//                 <td>{hat.style }</td>
//                 <td>{hat.color }</td>
//               </tr>

//             );
//           })}
//         </tbody>
//       </table>

//     );
// }
