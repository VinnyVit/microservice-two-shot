import React, { useState, useEffect } from 'react';

function ShoeList() {
    const [shoes, setShoes] = useState([]);

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        const response = await fetch('http://localhost:8080/api/shoes/');
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        } else {
            console.error('Failed to fetch shoe data');
        }
    };

    const handleDelete = async (id) => {
        const response = await fetch(`http://localhost:8080/api/shoes/${id}/`, { method: 'DELETE' });
        if (response.ok) {
            await fetchData();
        } else {
            console.error('Failed to delete shoe');
        }
    };

    return (
        <div>
            <h2>List of Shoes</h2>
            <div className="row">
                {shoes.map(shoe => (
                    <div key={shoe.id} className="col-md-3">
                        <div>
                            <img src={shoe.picture_url} alt={shoe.model_name} className="card-img-top" />
                        </div>
                        <div>
                            <h5>{shoe.manufacturer} - {shoe.model_name}</h5>
                            <button onClick={() => handleDelete(shoe.id)}>Delete</button>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default ShoeList;
