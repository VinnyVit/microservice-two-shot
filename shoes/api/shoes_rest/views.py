from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .models import Shoe, BinVO
from common.json import ModelEncoder
import json

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        'closet_name',
        'bin_number',
        'bin_size'
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        'manufacturer',
        'model_name',
        'color',
        'picture_url',
        'id'
    ]

@require_http_methods(["GET", "POST"])
def shoe_list(request):
    if request.method == 'GET':
        shoes = Shoe.objects.all()
        return JsonResponse({"shoes": shoes}, encoder=ShoeListEncoder)
    elif request.method == 'POST':
        data = json.loads(request.body)
        new_shoe = Shoe.objects.create(**data)
        return JsonResponse({'shoe': new_shoe.id}, status=201)

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        'manufacturer',
        'model_name',
        'color',
        'picture_url',
        'wardrobe_bin',
    ]
    encoders = {'wardrobe_bin':BinVOEncoder(),}

@require_http_methods(["GET", "DELETE", "PUT"])
def shoe_detail(request, id):
    try:
        if request.method == "GET":
            shoe = Shoe.objects.filter(id=id)
            if not shoe:
                return JsonResponse({"message": "Shoe not found"}, status=400)
            return JsonResponse({"shoe": shoe}, encoder=ShoeDetailEncoder)

        elif request.method == "DELETE":
            shoe = Shoe.objects.filter(id=id).delete()
            return JsonResponse({"deleted": True})

        else:
            content = json.loads(request.body)
            Shoe.objects.filter(id=id).update(**content)
            shoe = Shoe.objects.get(id=id)
            return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)

    except json.JSONDecodeError:
        return JsonResponse({"message": "Invalid JSON data"}, status=400)

@require_http_methods(['GET'])
def show_binvos(request):
    if request.method == 'GET':
        binvos = BinVO.objects.all()
        return JsonResponse({"binvos":binvos}, encoder=BinVOEncoder)
