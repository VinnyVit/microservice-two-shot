from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
# Create your models here.



class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length = 200, null=True)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)


    @classmethod
    def create(cls, **kwargs):
        location = cls(**kwargs)
        location.save()
        return location

class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(max_length=500,null = True)


    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE, null=True)

    @classmethod
    def create(cls, **kwargs):
        hat = cls(**kwargs)
        hat.save()
        return hat
    def get_api_url(self):
        return  reverse("api_show_hat",kwargs={"pk": self.pk} )
