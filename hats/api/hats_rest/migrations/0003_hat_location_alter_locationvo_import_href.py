# Generated by Django 4.0.3 on 2024-03-13 00:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0002_locationvo'),
    ]

    operations = [
        migrations.AddField(
            model_name='hat',
            name='location',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='hats', to='hats_rest.locationvo'),
        ),
        migrations.AlterField(
            model_name='locationvo',
            name='import_href',
            field=models.CharField(max_length=200, unique=True),
        ),
    ]
