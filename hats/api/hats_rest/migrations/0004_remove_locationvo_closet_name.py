# Generated by Django 4.0.3 on 2024-03-13 01:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0003_hat_location_alter_locationvo_import_href'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='locationvo',
            name='closet_name',
        ),
    ]
