from django.shortcuts import render
from django.views.decorators.http import require_http_methods
# Create your views here.
from common.json import ModelEncoder
import json
from hats_rest.models import Hat, LocationVO
from django.http import JsonResponse


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name", "section_number", "shelf_number"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style",
        "color",
        "picture_url"

    ]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style",
        "color",
        "picture_url",
        "location"
    ]
    encoders = {"location":LocationVOEncoder(),}



@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse({"hats": hats}, encoder=HatListEncoder)



    else:
        content = json.loads(request.body)
        try:

            location = LocationVO.objects.get(import_href=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location ID"},
                status=400,
            )


        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat, encoder=HatDetailEncoder, safe=False
        )



@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_hat(request,pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)


    if request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content= json.loads(request.body)
        Hat.objects.filter(id=pk).update(**content)
        hat = Hat.objects.get(id=pk)
        return JsonResponse( hat, encoder=HatDetailEncoder,safe=False)


@require_http_methods(["GET"])
def api_show_locationvos(request):
    if request.method == "GET":
        locationvo = LocationVO.objects.all()
        return JsonResponse({"location": locationvo}, encoder=LocationVOEncoder)
